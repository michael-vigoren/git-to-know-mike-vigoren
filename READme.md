### GIT to Know Mike Vigoren

## Overview:
Recently, I started looking for new roles outside my current one and created this repository and website as a way to showcase some of my skills and passions.  This project is to show the reader my  history, work experience, passions.

*  [Resume](Resume/Mike Vigoren Resume.pdf)
*  [Mike Vigoren History](https://gitlab.com/michael-vigoren/git-to-know-mike-vigoren/-/blob/master/MJV_PersonalHistory/Git_to_know_MJV.pdf)
*  [Code](https://gitlab.com/michael-vigoren/git-to-know-mike-vigoren/-/blob/master/Code/mjv_code.ipynb)
* [Hire Me](https://youtu.be/fuRcC_TwNm4)
* [My website](https://mikevigoren.com/)



Please submit any comments, questions, or suggestions to the Suggestion folder.

Thank you for looking!


